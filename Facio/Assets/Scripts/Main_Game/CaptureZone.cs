﻿using UnityEngine;
using System.Collections;

public class CaptureZone : MonoBehaviour
{
	private ParticleSystem confettiParticles;

	void Awake()
	{
		confettiParticles = GameObject.Find("Particle System").GetComponent<ParticleSystem>();
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		bool isPrize = false;

		switch (col.gameObject.tag)
		{
		case Constants.prize_logo:
			isPrize = true;
			GameManager.GetInstance.RegisterCapturedPrize(prizeTypeEnum.Logo);
			break;
		case Constants.prize_pouch:
			isPrize = true;
			GameManager.GetInstance.RegisterCapturedPrize(prizeTypeEnum.Pouch);
			break;
		case Constants.prize_flap:
			isPrize = true;
			GameManager.GetInstance.RegisterCapturedPrize(prizeTypeEnum.Flap);
			break;
		case Constants.prize_sling:
			isPrize = true;
			GameManager.GetInstance.RegisterCapturedPrize(prizeTypeEnum.Sling);
			break;
		case Constants.prize_bag:
			isPrize = true;
			GameManager.GetInstance.RegisterCapturedPrize(prizeTypeEnum.Bag);
			break;
		default:
			isPrize = false;
			break;
		}
		
		if (isPrize == true)
		{
			TriggerCelebration();
			Destroy(col.gameObject);
		}
	}

	private void TriggerCelebration()
	{
		Audio_Manager.PlayPrizeGetSound();
		confettiParticles.Stop();
		confettiParticles.Play();
	}
}
