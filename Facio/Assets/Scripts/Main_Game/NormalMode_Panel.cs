﻿using UnityEngine;
using System.Collections;

public class NormalMode_Panel : MonoBehaviour
{
	public void BackButton()
	{
		Audio_Manager.PlayButtonSound();
		Audio_Manager.ResetPausedSounds();

		Application.LoadLevel(0);
	}
	
	public void OKButton()
	{
		Audio_Manager.PlayButtonSound();

		if (GameManager.GetInstance.bIsPaused == true)
			GameManager.GetInstance.UnPause();
	}
	
	public void PauseButton()
	{
		Audio_Manager.PlayButtonSound();

		GameManager.GetInstance.Pause();
	}
}
