using UnityEngine;
using System.Collections;

public class Claw_MoveHorizontally : IClawState
{
	private float accidentalTouchTolerence = 0.5f;
	private float initialTouchTime;
	private bool resettingPosition = false;

	public override void Enter()
	{
		if (m_ClawFSM.m_bShouldMoveLeft == true)
		{
			initialTouchTime = Time.time;
		}
	}
	
	public override void Execute()
	{
		if (resettingPosition == true)
		{
			ResetPosition();
		}
		else
		{
			if (m_ClawFSM.m_bShouldMoveRight == true)
			{
				MoveRight();
			}
			else if (m_ClawFSM.m_bShouldMoveLeft == true)
			{
				Audio_Manager.PlayClawMoveSound(true);
				MoveLeft();
			}
			else if (m_ClawFSM.m_bMovedLeft == false) // Just ended move
			{	
				if ((Time.time - initialTouchTime) < accidentalTouchTolerence)
				{
					resettingPosition = true;
				}
				else
				{
					Audio_Manager.PlayClawMoveSound(false);

					// Confirm the move.
					m_ClawFSM.m_bMovedLeft = true;
					m_ClawFSM.m_bShouldMoveLeft = false;
					
					// Transition to Open Claw.
					m_ClawFSM.m_bShouldOpen = true;
					m_ClawFSM.DelayedChangeState(clawStateEnum.MoveJaws);
				}
			}
		}



		// Check for deferred state change.
		if (m_ClawFSM.m_bHasAwaitingDeferredStateChange == true)
		{
			m_ClawFSM.ExecuteDeferredStateChange();
		}
	}
	
	public override void Exit()
	{
		resettingPosition = false;
	}
	
	// Constructor
	public Claw_MoveHorizontally(Claw_FSM clawFSM)
	{
		m_ClawFSM = clawFSM;
	}






	#region Helper functions
	private void ResetPosition()
	{
		// Move back to position
		float step = m_ClawFSM.m_fClawHorizontalMovementSpeed * Time.deltaTime;
		m_ClawFSM.transform.position = Vector3.MoveTowards(m_ClawFSM.transform.position, m_ClawFSM.m_OriginPos, step);
		
		// Transition to idle once done.
		if (m_ClawFSM.transform.position == m_ClawFSM.m_OriginPos)
		{
			// Give back the try.
			PlayerPrefs.SetInt(Constants.triesKey, GameManager.GetInstance.numTries);
			m_ClawFSM.ChangeState(clawStateEnum.Idle);

			Audio_Manager.PlayClawMoveSound(false);
		}
	}
	
	private void MoveLeft()
	{
		if (m_ClawFSM.transform.position.x < -6.45f)	return;
		m_ClawFSM.transform.position += Vector3.right * -m_ClawFSM.m_fClawHorizontalMovementSpeed * Time.deltaTime;
	}

	private void MoveRight()
	{
		Audio_Manager.PlayClawMoveSound(true);

		// Move back to position
		float step = m_ClawFSM.m_fClawHorizontalMovementSpeed * Time.deltaTime;
		m_ClawFSM.transform.position = Vector3.MoveTowards(m_ClawFSM.transform.position, m_ClawFSM.m_OriginPos, step);

		if (m_ClawFSM.transform.position == m_ClawFSM.m_OriginPos)
		{
			Audio_Manager.PlayClawMoveSound(false);
			m_ClawFSM.m_bMovedRight = true;

			// Open Jaws
			m_ClawFSM.m_bShouldOpen = true;
			m_ClawFSM.DelayedChangeState(clawStateEnum.MoveJaws);
		}
	}
	#endregion
}

