using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Claw_FSM : MonoBehaviour
{
	// FSM variables
	private clawStateEnum m_CurrentEnumState;
	private IClawState m_CurrentState;
	private Dictionary<clawStateEnum, IClawState> m_ClawStatesDictionary;

	// UI related variables
	private Rect m_ScreenRightArea;
	private bool m_bClawButtonIsBeingTouched;

	// For Deferred state change.
	[HideInInspector]
	public bool m_bHasAwaitingDeferredStateChange = false;
	private clawStateEnum m_deferredState;

	// Component Caching
	public HingeJoint2D m_leftJawHinge, m_rightJawHinge;
	public Rigidbody2D m_clawRB;

	// Machine variables
	public bool m_bMovedLeft = false;
	public bool m_bShouldMoveLeft = false; // Used to tell if the player has pressed on the screen button.
	public bool m_bMovedRight = false;
	public bool m_bShouldMoveRight = false;
	public bool m_bShouldMoveUp = false;
	public bool m_bMovedUp = false;
	public bool m_bShouldMoveDown = false;
	public bool m_bMovedDown = false;
	public bool m_bShouldOpen = false;
	public bool m_bIsOpen = false;
	public bool m_bShouldClose = false;
	public bool m_bIsClosed = false;
	public Vector3 m_OriginPos;
	public float m_fClawOpenCloseSpeed = 100f;
	public float m_fClawMaxMotorForce = 200.0f;
	public float m_fClawSmallMotorForce = 4.0f;
	public float m_fClawLowerAngleLimit = 55.0f;
	public float m_fClawUpperAngleLimit = 100.0f;
	public float m_fClawHorizontalMovementSpeed = 2.5f;
	public float m_fClawVerticalMovementSpeed = 1f;

	#region MonoBehaviour
	void Awake()
	{
		m_ClawStatesDictionary = new Dictionary<clawStateEnum, IClawState>();
		m_ClawStatesDictionary.Add(clawStateEnum.Idle, new Claw_IdleState(this));
		m_ClawStatesDictionary.Add(clawStateEnum.MoveJaws, new Claw_MoveJaws(this));
		m_ClawStatesDictionary.Add(clawStateEnum.MoveVertically, new Claw_MoveVertically(this));
		m_ClawStatesDictionary.Add(clawStateEnum.MoveHorizontally, new Claw_MoveHorizontally(this));
		m_ClawStatesDictionary.Add(clawStateEnum.Delay, new Claw_DelayState(this));

		m_CurrentEnumState = clawStateEnum.Idle;
		m_CurrentState = m_ClawStatesDictionary[m_CurrentEnumState];

		m_bClawButtonIsBeingTouched = false;
		m_OriginPos = transform.position;
	}

	void Start()
	{
		// Has to be in start because screen size is still being set up during Awake() call in Camera_Resizer.cs
		m_ScreenRightArea = new Rect(Screen.width/2, 0, Screen.width/2, Screen.height);
	}
	
	void Update()
	{
		if (GameManager.GetInstance.bGameStarted == false) return;

		if (m_bMovedLeft == false)
		{
			// Player has tapped the screen already and is still holding onto the screen somwhere.
			if (Input.touchCount > 0 && m_bClawButtonIsBeingTouched == true)
			{
				// Go through all the touches
				bool bHasOneFingerWithinArea = false;
				for (int i = 0; i < Input.touchCount; i++)
				{
					Vector2 touchPos = Input.GetTouch(i).position;
					if (m_ScreenRightArea.Contains(touchPos))
					{
						bHasOneFingerWithinArea = true;
						break;
					}
				}

				if (bHasOneFingerWithinArea == false)
				{
					m_bClawButtonIsBeingTouched = false;
					m_bShouldMoveLeft = false;
				}
			}
			// If player JUST touched the screen.
			else if (Input.touchCount > 0 && m_bClawButtonIsBeingTouched == false)
			{
				// Go through all the touches
				for (int i = 0; i < Input.touchCount; i++)
				{
					// Just have to find one touch that is in the area.
					Vector2 touchPos = Input.GetTouch(i).position;
					if (m_ScreenRightArea.Contains(touchPos))
					{
						if (GameManager.GetInstance.numTries > 0)
						{
							m_bClawButtonIsBeingTouched = true;
							m_bShouldMoveLeft = true;
//							int numTriesLeft = GameManager.GetInstance.numTries - 1;
//							PlayerPrefs.SetInt(Constants.triesKey, numTriesLeft);
							GameManager.GetInstance.CheatPreventionUseTry();
							break;
						}
					}
				}
			}
			else
			{
				m_bClawButtonIsBeingTouched = false;
				m_bShouldMoveLeft = false;
			}

			#if UNITY_EDITOR
			// PC CONTROLS
			if (Input.GetKey(KeyCode.Space))
			{
				m_bClawButtonIsBeingTouched = true;
				if (GameManager.GetInstance.numTries > 0)
				{
//					int numTriesLeft = GameManager.GetInstance.numTries - 1;
//					PlayerPrefs.SetInt(Constants.triesKey, numTriesLeft);
					GameManager.GetInstance.CheatPreventionUseTry();
					m_bShouldMoveLeft = true;
				}
			}
			else
			{
				m_bClawButtonIsBeingTouched = false;
				m_bShouldMoveLeft = false;
			}
			#endif
		}

		m_CurrentState.Execute();
	}
	#endregion


	



	#region Functions
	public void ChangeState(clawStateEnum newState)
	{
		m_CurrentState.Exit();
		m_CurrentState = m_ClawStatesDictionary[newState];
		m_CurrentState.Enter();
		m_CurrentEnumState = newState;
	}

	public void DelayedChangeState(clawStateEnum newState)
	{
		(m_ClawStatesDictionary[clawStateEnum.Delay] as Claw_DelayState).stateToChangeTo = newState;
		m_CurrentState.Exit();
		m_CurrentState = m_ClawStatesDictionary[clawStateEnum.Delay];
		m_CurrentState.Enter();
		m_CurrentEnumState = clawStateEnum.Delay;
	}

	public void ResetClaw()
	{
		m_bMovedLeft = false;
		m_bShouldMoveLeft = false;
		m_bMovedRight = false;
		m_bShouldMoveRight = false;
		m_bShouldMoveUp = false;
		m_bMovedUp = false;
		m_bShouldMoveDown = false;
		m_bMovedDown = false;
		m_bShouldOpen = false;
		m_bIsOpen = false;
		m_bShouldClose = false;
		m_bIsClosed = false;

		// Change back to idle state.
		DelayedChangeState(clawStateEnum.Idle);

		// Update the tries
		GameManager.GetInstance.UseTry();
	}

	// Deferred state change.
	public void DeferredChangeState(clawStateEnum newState)
	{
		m_deferredState = newState;
		m_bHasAwaitingDeferredStateChange = true;
	}
	
	public void ExecuteDeferredStateChange()
	{
		ChangeState(m_deferredState);
		m_bHasAwaitingDeferredStateChange = false;
	}
	#endregion
}
