﻿using UnityEngine;
using System.Collections;

public class Claw_IdleState : IClawState
{
	public override void Enter()
	{

	}

	public override void Execute()
	{
		// Check if need to transisition to MoveHorizontally state.
		if (m_ClawFSM.m_bShouldMoveLeft == true)
		{
			m_ClawFSM.ChangeState(clawStateEnum.MoveHorizontally);
		}

		// Check for deferred state change.
		if (m_ClawFSM.m_bHasAwaitingDeferredStateChange == true)
		{
			m_ClawFSM.ExecuteDeferredStateChange();
		}
	}

	public override void Exit()
	{

	}

	// Constructor
	public Claw_IdleState(Claw_FSM clawFSM)
	{
		m_ClawFSM = clawFSM;
	}
}
