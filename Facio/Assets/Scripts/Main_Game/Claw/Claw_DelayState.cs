﻿using UnityEngine;
using System.Collections;

public class Claw_DelayState : IClawState
{
	private float entryTime;
	private float delayTime = 1.0f;

	public clawStateEnum stateToChangeTo;

	public override void Enter()
	{
		entryTime = Time.time;
	}
	
	public override void Execute()
	{
		if ((Time.time - entryTime) > delayTime)
		{
			m_ClawFSM.ChangeState(stateToChangeTo);
		}
	}
	
	public override void Exit()
	{
		
	}
	
	// Constructor
	public Claw_DelayState(Claw_FSM clawFSM)
	{
		m_ClawFSM = clawFSM;
	}
}
