﻿using UnityEngine;
using System.Collections;

public class Claw_MoveJaws : IClawState
{
	private float entryTime;
	private float openCloseTime = 1.5f;

	public override void Enter()
	{
		entryTime = Time.time;
	}
	
	public override void Execute()
	{
		if (m_ClawFSM.m_bShouldOpen == true)
		{
			if (m_ClawFSM.m_bIsOpen == false)
			{
				JointAngleLimits2D newLimits = new JointAngleLimits2D();
				newLimits.min = -m_ClawFSM.m_fClawLowerAngleLimit;
				newLimits.max = 0;
				m_ClawFSM.m_rightJawHinge.limits = newLimits;

				newLimits.min = 0;
				newLimits.max = m_ClawFSM.m_fClawLowerAngleLimit;
				m_ClawFSM.m_leftJawHinge.limits = newLimits;

				OpenJaw();

				m_ClawFSM.m_clawRB.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionY;
				m_ClawFSM.m_clawRB.drag = 100.0f;
				m_ClawFSM.m_clawRB.isKinematic = false;
			}
			else
			{
				// Check the time.
				if ((Time.time - entryTime) > openCloseTime)
				{
					m_ClawFSM.m_bShouldOpen = false;

					m_ClawFSM.m_clawRB.constraints = RigidbodyConstraints2D.FreezeRotation;
					m_ClawFSM.m_clawRB.drag = 0f;
					m_ClawFSM.m_clawRB.isKinematic = true;

					LooselyKeepJawOpen();

					JointAngleLimits2D newLimits = new JointAngleLimits2D();
					newLimits.min = -m_ClawFSM.m_fClawUpperAngleLimit;
					newLimits.max = 0;
					m_ClawFSM.m_rightJawHinge.limits = newLimits;
					
					newLimits.min = 0;
					newLimits.max = m_ClawFSM.m_fClawUpperAngleLimit;
					m_ClawFSM.m_leftJawHinge.limits = newLimits;

					// Change state.
					if (m_ClawFSM.m_bMovedRight == false) // The first move down not done yet.
					{
						m_ClawFSM.m_bShouldMoveDown = true;
						m_ClawFSM.ChangeState(clawStateEnum.MoveVertically);
					}
					else // Is at the right, just close.
					{
						m_ClawFSM.m_bShouldClose = true;
					}
				}
			}
		}
		else if (m_ClawFSM.m_bShouldClose == true)
		{
			if (m_ClawFSM.m_bIsClosed == false)
			{
				CloseJaw();
			}
			else
			{
				// Check the time.
				if ((Time.time - entryTime) > openCloseTime)
				{
					m_ClawFSM.m_bShouldClose = false;

					// Change state.
					if (m_ClawFSM.m_bMovedRight == false) // The first move up not done yet.
					{
						m_ClawFSM.m_bShouldMoveUp = true;
						m_ClawFSM.ChangeState(clawStateEnum.MoveVertically);
					}
					else
					{
						// Done. Change to idle and reset all the booleans.
						m_ClawFSM.ResetClaw();
					}
				}
			}
		}

		// Check for deferred state change.
		if (m_ClawFSM.m_bHasAwaitingDeferredStateChange == true)
		{
			m_ClawFSM.ExecuteDeferredStateChange();
		}
	}
	
	public override void Exit()
	{
		
	}
	
	// Constructor
	public Claw_MoveJaws(Claw_FSM clawFSM)
	{
		m_ClawFSM = clawFSM;
	}








	#region Helper functions
	public void OpenJaw()
	{
		Audio_Manager.PlayClawOpenCloseSound();

		JointMotor2D motor = m_ClawFSM.m_leftJawHinge.motor;
		motor.motorSpeed = m_ClawFSM.m_fClawOpenCloseSpeed;
		m_ClawFSM.m_leftJawHinge.motor = motor;

		motor = m_ClawFSM.m_rightJawHinge.motor;
		motor.motorSpeed = -m_ClawFSM.m_fClawOpenCloseSpeed;
		m_ClawFSM.m_rightJawHinge.motor = motor;

		m_ClawFSM.m_bIsOpen = true;
		m_ClawFSM.m_bIsClosed = false;
	}

	public void LooselyKeepJawOpen()
	{
		JointMotor2D motor = m_ClawFSM.m_leftJawHinge.motor;
		motor.motorSpeed = -1.0f;
		motor.maxMotorTorque = m_ClawFSM.m_fClawSmallMotorForce;
		m_ClawFSM.m_leftJawHinge.motor = motor;
		
		motor = m_ClawFSM.m_rightJawHinge.motor;
		motor.motorSpeed = 1.0f;
		motor.maxMotorTorque = m_ClawFSM.m_fClawSmallMotorForce;
		m_ClawFSM.m_rightJawHinge.motor = motor;
	}

	public void CloseJaw()
	{
		Audio_Manager.PlayClawOpenCloseSound();

		JointMotor2D motor = m_ClawFSM.m_leftJawHinge.motor;
		motor.motorSpeed = -m_ClawFSM.m_fClawOpenCloseSpeed;
		motor.maxMotorTorque = m_ClawFSM.m_fClawMaxMotorForce;
		m_ClawFSM.m_leftJawHinge.motor = motor;
		
		motor = m_ClawFSM.m_rightJawHinge.motor;
		motor.motorSpeed = m_ClawFSM.m_fClawOpenCloseSpeed;
		motor.maxMotorTorque = m_ClawFSM.m_fClawMaxMotorForce;
		m_ClawFSM.m_rightJawHinge.motor = motor;

		m_ClawFSM.m_bIsClosed = true;
		m_ClawFSM.m_bIsOpen = false;
	}
	#endregion
}
