﻿using UnityEngine;
using System.Collections;

public class Claw_MoveVertically : IClawState
{
	private float moveDownDuration = 3.0f;
	private float entryTime;
	private Vector3 verticalOrigin;

	public override void Enter()
	{
		verticalOrigin = new Vector3(m_ClawFSM.transform.position.x, m_ClawFSM.m_OriginPos.y, 0);
		entryTime = Time.time;
	}
	
	public override void Execute()
	{
		if (m_ClawFSM.m_bShouldMoveDown == true)
		{
			if (m_ClawFSM.m_bMovedDown == true)
			{
				// After moving down, close the jaws.
				m_ClawFSM.m_bShouldClose = true;
				m_ClawFSM.ChangeState(clawStateEnum.MoveJaws);
			}
			else
			{
				MoveDown();
			}
		}
		else if (m_ClawFSM.m_bShouldMoveUp == true)
		{
			if (m_ClawFSM.m_bMovedUp == true)
			{
				// After moving back up, move right.
				m_ClawFSM.m_bShouldMoveRight = true;
				m_ClawFSM.ChangeState(clawStateEnum.MoveHorizontally);
			}
			else
			{
				MoveUp();
			}
		}

		// Check for deferred state change.
		if (m_ClawFSM.m_bHasAwaitingDeferredStateChange == true)
		{
			m_ClawFSM.ExecuteDeferredStateChange();
		}
	}
	
	public override void Exit()
	{
		m_ClawFSM.m_bShouldMoveUp = false;
		m_ClawFSM.m_bMovedUp = false;
		m_ClawFSM.m_bShouldMoveDown = false;
		m_ClawFSM.m_bMovedDown = false;
	}
	
	// Constructor
	public Claw_MoveVertically(Claw_FSM clawFSM)
	{
		m_ClawFSM = clawFSM;
	}





	#region Helper Functions
	private void MoveDown()
	{
		m_ClawFSM.m_clawRB.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX;
		m_ClawFSM.m_clawRB.isKinematic = false;
		m_ClawFSM.m_clawRB.gravityScale = 1.0f;

		Audio_Manager.PlayClawLowerSound(true);

		if ((Time.time - entryTime) > moveDownDuration)
		{
			m_ClawFSM.m_clawRB.constraints = RigidbodyConstraints2D.FreezeRotation;
			m_ClawFSM.m_clawRB.isKinematic = true;
			m_ClawFSM.m_clawRB.gravityScale = 0.0f;

			m_ClawFSM.m_bMovedDown = true;
			Audio_Manager.PlayClawLowerSound(false);
		}
	}

	private void MoveUp()
	{
		Audio_Manager.PlayClawLowerSound(true);

		float step = m_ClawFSM.m_fClawVerticalMovementSpeed * Time.deltaTime;
		m_ClawFSM.transform.position = Vector3.MoveTowards(m_ClawFSM.transform.position, verticalOrigin, step);

		if (m_ClawFSM.transform.position == verticalOrigin)
		{
			Audio_Manager.PlayClawLowerSound(false);

			m_ClawFSM.m_bMovedUp = true;
		}
	}
	#endregion
}

