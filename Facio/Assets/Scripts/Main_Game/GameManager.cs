﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class GameManager : MonoBehaviour
{
	private static GameManager s_Instance;
	public static GameManager GetInstance {get{return s_Instance;}}

	private int nTriesLeft = 5;
	public int numTries {get{return nTriesLeft;}}
	public GameMode gameMode = GameMode.Normal;
	public bool bIsPaused = false;
	public bool bGameStarted = false;
	private int nNumPrizes = 0;
	public Transform LeftSpawn, RightSpawn;
	public GameObject[] prizePrefabs;

	private Text tryText;
	private CanvasGroup endGamePanel;
	private Text endGameText, backBtnText, okBtnText;
	private SpriteRenderer instructionsBackgroundRen;

	void Awake()
	{
		// Singleton implementation.
		if (s_Instance == null)
			s_Instance = this;
		else
			Destroy(this.gameObject);

		// Setting up of references.
		tryText = GameObject.Find("Claw_Tries_Text").GetComponent<Text>();
		endGamePanel = GameObject.Find("Panel").GetComponent<CanvasGroup>();
		endGameText = endGamePanel.transform.GetChild(0).GetComponent<Text>();
		backBtnText = endGamePanel.transform.GetChild(1).GetChild(0).GetComponent<Text>();
		okBtnText = endGamePanel.transform.GetChild(2).GetChild(0).GetComponent<Text>();
		instructionsBackgroundRen = GameObject.Find("UI_InstructionsBackground").GetComponent<SpriteRenderer>();

		SetPanelVisibility(false);
		instructionsBackgroundRen.enabled = false;

		switch (gameMode)
		{
		case GameMode.Booth:
			nTriesLeft = 5;
			break;
		case GameMode.Normal:
			if (PlayerPrefs.HasKey(Constants.triesKey) == true)
			{
				if (PlayerPrefs.HasKey(Constants.LastRechargeTimeKey) == false)
				{
					nTriesLeft = 5;
					PlayerPrefs.SetInt(Constants.triesKey, nTriesLeft);
					break;
				}
				nTriesLeft = PlayerPrefs.GetInt(Constants.triesKey);
				if (nTriesLeft < 5 && PlayerPrefs.HasKey(Constants.LastRechargeTimeKey) == true)
				{
					DecodeTime();
					PlayerPrefs.SetInt(Constants.triesKey, nTriesLeft); // Save the new num of tries after recharge calculation.
				}
			}
			else
			{
				PlayerPrefs.SetInt(Constants.triesKey, nTriesLeft);
			}
			break;
		}

		// FORCE TIMESCALE TO RESET UPON LOAD.
		Time.timeScale = 1f;
	}

	public void Start()
	{
		UpdateTryText();

		switch (gameMode)
		{
		case GameMode.Booth:
			// Spawn the first two prizes.
			GameObject.Instantiate(prizePrefabs[(int)prizeTypeEnum.Flap], LeftSpawn.position, Quaternion.identity);
			GameObject.Instantiate(prizePrefabs[(int)prizeTypeEnum.Sling], RightSpawn.position, Quaternion.Euler(new Vector3(0, 0, -80)));
			bGameStarted = true;
			instructionsBackgroundRen.enabled = true;
			break;
		case GameMode.Normal:
			// Spawn ten random
			SpawnNormalPrizes();
			break;
		}
	}








	// DEBUG
	#if UNITY_EDITOR
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Minus))
		{
			int numTriesLeft = GameManager.GetInstance.numTries - 1;
			PlayerPrefs.SetInt(Constants.triesKey, numTriesLeft);
			UseTry();
		}
	}
	#endif









	public void SpawnNormalPrizes()
	{
		int[] prizeType = {0, 1, 2, 3, 4, 0};
//		prizeType[prizeType.Length-1] = UnityEngine.Random.Range(0, 5); // Set random prize.
		int currentIndex;
		int temp;

		for (int i = 0; i < 100; i++)
		{
			currentIndex = UnityEngine.Random.Range(0, prizeType.Length);
			temp = prizeType[0];
			prizeType[0] = prizeType[currentIndex];
			prizeType[currentIndex] = temp;
		}

		StartCoroutine(InstantiatePrizesWithInterval(prizeType));
	}

	private IEnumerator InstantiatePrizesWithInterval(int[] _prizeType)
	{
		int[] spawnPos = {1, 0, 1, 0, 0, 0};
		Vector3 pos;

		for (int i = 0; i < _prizeType.Length; i++)
		{
			if (spawnPos[i] == 0) // Spawn left
			{
				pos = LeftSpawn.position;
				pos.x += UnityEngine.Random.Range(-1.0f, 2.0f);
			}
			else // Spawn right
			{
				pos = RightSpawn.position;
				pos.x += UnityEngine.Random.Range(-2.0f, -1.0f);
			}

			GameObject.Instantiate(prizePrefabs[_prizeType[i]], pos, Quaternion.Euler(new Vector3 (0, 0, UnityEngine.Random.Range(0f, 360f))));

			yield return new WaitForSeconds(0.75f);
		}

		instructionsBackgroundRen.enabled = true;
		bGameStarted = true;
	}

	#region Helper Functions
	private void UpdateTryText()
	{
		tryText.text = Convert.ToString(nTriesLeft);
	}

	public void CheatPreventionUseTry()
	{
		if (gameMode != GameMode.Normal)
			return;

		if (nTriesLeft > 0)
		{
			PlayerPrefs.SetInt(Constants.triesKey, nTriesLeft - 1);

			if (PlayerPrefs.HasKey(Constants.LastRechargeTimeKey) == false)
			{
				PlayerPrefs.SetString(Constants.LastRechargeTimeKey, System.DateTime.Now.ToBinary().ToString());
			}
			else if (nTriesLeft - 1 == 4)
			{
				PlayerPrefs.SetString(Constants.LastRechargeTimeKey, System.DateTime.Now.ToBinary().ToString());
			}
		}
	}

	public bool UseTry()
	{
		if (nTriesLeft > 0)
		{
			nTriesLeft--;
			UpdateTryText();

			switch (gameMode)
			{
			case GameMode.Booth:
				if (nTriesLeft == 0)
					Invoke("EndBoothMode", 1.0f);
				break;
			case GameMode.Normal:
				if (PlayerPrefs.HasKey(Constants.LastRechargeTimeKey) == false)
				{
					PlayerPrefs.SetString(Constants.LastRechargeTimeKey, System.DateTime.Now.ToBinary().ToString());
				}
				else if (nTriesLeft == 4)
				{
					PlayerPrefs.SetString(Constants.LastRechargeTimeKey, System.DateTime.Now.ToBinary().ToString());
				}

				if (nTriesLeft == 0)
				{
					if (PlayerPrefs.HasKey(Constants.HasUsedFacebookRefillKey) == false)
					{
						GameObject.Find("Refill_PromoText").GetComponent<Text>().enabled = true;
					}
					else
					{
						GameObject.Find("Refill_PromoText").GetComponent<Text>().text = "You have run out of tries! Please wait for your tries to recharge. Each try takes one hour to recharge.";
						GameObject.Find("Refill_PromoText").GetComponent<Text>().enabled = true;
					}
				}
				break;
			}

			return true;
		}
		else
			return false;
	}

	public void DecodeTime()
	{
		//Store the current time when it starts
		DateTime currentDate = System.DateTime.Now;
		//Grab the old time from the player prefs as a long
		Int64 temp = Convert.ToInt64(PlayerPrefs.GetString(Constants.LastRechargeTimeKey));
		//Convert the old time from binary to a DataTime variable
		DateTime oldDate = DateTime.FromBinary(temp);
		//Use the Subtract method and store the result as a timespan variable
		TimeSpan difference = currentDate.Subtract(oldDate);


		int secsSinceLastRecharge = Convert.ToInt32(difference.TotalSeconds);
		int numHours = secsSinceLastRecharge / Constants.secondsPerHour;
		nTriesLeft += numHours;


		//Debug.Log(secsSinceLastRecharge + " / " + Constants.secondsPerHour + " : " + numHours);

		if (nTriesLeft > 5)
		{
			nTriesLeft = 5;
		}
		else if (nTriesLeft < 5 && numHours > 0) // Bring over the leftover time.
		{
			DateTime newRechargeTime = System.DateTime.Now;
			newRechargeTime.AddSeconds(-(secsSinceLastRecharge % Constants.secondsPerHour));
			PlayerPrefs.SetString(Constants.LastRechargeTimeKey, newRechargeTime.ToBinary().ToString());
		}
	}

	public void SetPanelVisibility(bool visible)
	{
		if (visible == true)
		{
			endGamePanel.alpha = 1;
			endGamePanel.interactable = true;
			endGamePanel.blocksRaycasts = true;
		}
		else
		{
			endGamePanel.alpha = 0;
			endGamePanel.interactable = false;
			endGamePanel.blocksRaycasts = false;
		}
	}

	public void EndBoothMode()
	{
		if (nNumPrizes == 0)
		{
			endGameText.text = "That's too bad. Thanks for trying! Don't worry, you can still buy a Facio bag regardless. Why don't you do that now since you are at the Facio booth!";
		}
		else if (nNumPrizes < 5)
		{
			endGameText.text = "Good try! Congratulations you've earned yourself a $" + nNumPrizes / 2 + "." + ((nNumPrizes % 2) * 5) + "0 discount for your purchase of a Facio Bag! Enjoy!";
		}
		else
		{
			endGameText.text = "WOW! You got all of the Facio bag parts! Congratulations! here's a well deserved $" + nNumPrizes / 2 + "." + ((nNumPrizes % 2) * 5) + "0 discount for your purchase of a Facio Bag! Enjoy!";
		}
		backBtnText.text = "MENU ";
		okBtnText.text = " RESET";

		GameObject.Find("Panel_OK_Button").GetComponent<Button>().interactable = false;
		GameObject.Find("Panel_Back_Button").GetComponent<Button>().interactable = false;
		Invoke("EnableButtons", 2f);

		SetPanelVisibility(true);
	}

	public void EnableButtons()
	{
		GameObject.Find("Panel_OK_Button").GetComponent<Button>().interactable = true;
		GameObject.Find("Panel_Back_Button").GetComponent<Button>().interactable = true;
	}

	public void RegisterCapturedPrize(prizeTypeEnum _prizeType)
	{
		switch (gameMode)
		{
		case GameMode.Normal:
			PlayerPrefs.SetInt(Constants.prizeKey[(int)_prizeType], 1);
			break;
		case GameMode.Booth:
			nNumPrizes++;
			if (nNumPrizes == 2)
			{
				// Spawn the next two.
				GameObject.Instantiate(prizePrefabs[(int)prizeTypeEnum.Bag], LeftSpawn.position, Quaternion.Euler(new Vector3(0, 0, 90)));
				GameObject.Instantiate(prizePrefabs[(int)prizeTypeEnum.Pouch], RightSpawn.position, Quaternion.identity);
			}
			else if (nNumPrizes == 4)
			{
				// Spawn the last prize
				GameObject.Instantiate(prizePrefabs[(int)prizeTypeEnum.Logo], RightSpawn.position, Quaternion.identity);
			}
			break;
		}
	}

	public void Pause()
	{
		bIsPaused = true;
		Time.timeScale = 0f;

		switch (gameMode)
		{
		case GameMode.Normal:
			endGameText.text = "You have paused the game. Click RESUME to continue. Otherwise, click MENU to return to the main menu.";
			backBtnText.text = "MENU ";
			okBtnText.text = " RESUME";
			
			SetPanelVisibility(true);
			break;
		case GameMode.Booth:
			endGameText.text = "You have paused the game. Click RESUME to continue. Otherwise, click MENU to return to the main menu.";
			backBtnText.text = "MENU ";
			okBtnText.text = " RESUME";
			
			SetPanelVisibility(true);
			break;
		}
	}

	public void UnPause()
	{
		bIsPaused = false;
		Time.timeScale = 1f;


		switch (gameMode)
		{
		case GameMode.Normal:
			SetPanelVisibility(false);
			break;
		case GameMode.Booth:
			SetPanelVisibility(false);
			break;
		}
	}
	#endregion
}
