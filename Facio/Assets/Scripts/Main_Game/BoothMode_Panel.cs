﻿using UnityEngine;
using System.Collections;

public class BoothMode_Panel : MonoBehaviour
{
	public void BackButton()
	{
		Audio_Manager.PlayButtonSound();

		Application.LoadLevel(0);
	}

	public void OKButton()
	{
		Audio_Manager.PlayButtonSound();

		if (GameManager.GetInstance.numTries == 0 && GameManager.GetInstance.bIsPaused == false)
		{
			Audio_Manager.ResetPausedSounds();
			Application.LoadLevel(2);
		}
		else if (GameManager.GetInstance.bIsPaused == true)
			GameManager.GetInstance.UnPause();
	}

	public void PauseButton()
	{
		Audio_Manager.PlayButtonSound();

		if (GameManager.GetInstance.numTries != 0)
			GameManager.GetInstance.Pause();
	}
}
