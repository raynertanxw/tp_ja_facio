﻿using UnityEngine;
using System.Collections;

public static class Constants
{
	#region keys
	public static string redeemKey = "RedeemKey";
	public static string[] prizeKey = {"prize_logo_key", "prize_pouch_key", "prize_flap_key", "prize_sling_key", "prize_bag_key"};
	public static string triesKey = "TriesKey";
	public static string LastRechargeTimeKey = "LastRechargeTimeKey";
	public static string MusicEnabledKey = "MusicEnabledKey";
	public static string SoundEffectEnabledKey = "SoundEffectEnabledKey";
	public static string HasPlayedBeforeKey = "HasPlayedBeforeKey";
	public static string HasUsedFacebookRefillKey = "HadUsedFaceboookRefillKey";
	#endregion

	#region tags
	public const string prize_logo = "Prize_Logo";
	public const string prize_pouch = "Prize_Pouch";
	public const string prize_flap = "Prize_Flap";
	public const string prize_sling = "Prize_Sling";
	public const string prize_bag = "Prize_Bag";
	#endregion

	#region Maths
	public const int secondsPerHour = 60*60;
	#endregion
}
