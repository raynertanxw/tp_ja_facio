﻿public abstract class IState
{
	public virtual void Enter() {}
	public virtual void Execute() {}
	public virtual void Exit() {}
}

public abstract class IClawState : IState
{
	protected Claw_FSM m_ClawFSM = null;
}