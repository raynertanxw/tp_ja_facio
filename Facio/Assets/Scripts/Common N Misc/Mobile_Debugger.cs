﻿using UnityEngine;
using System.Collections;
using System;

public class Mobile_Debugger : MonoBehaviour {
	
	private static string debugConstructionText = "";
	private static string debugText = "";
	private GUIStyle debugBoxStyle;
	
	public bool debugIsOn = false;

	#region Overloaded Log Functions
	public static void Log(string newString)
	{
		debugConstructionText = debugConstructionText + newString + "\n";
		UnityEngine.Debug.Log(newString);
	}

	public static void Log(int newInt)
	{
		debugConstructionText = debugConstructionText + Convert.ToString(newInt) + "\n";
		UnityEngine.Debug.Log(newInt);
	}

	public static void Log(float newFloat)
	{
		debugConstructionText = debugConstructionText + Convert.ToString(newFloat) + "\n";
		UnityEngine.Debug.Log(newFloat);
	}

	public static void Log(Vector2 newVector2)
	{
		debugConstructionText = debugConstructionText + "(" + Convert.ToString(newVector2.x) + ", " + Convert.ToString(newVector2.y) + ")\n";
		UnityEngine.Debug.Log(newVector2);
	}

	public static void Log(Vector3 newVector3)
	{
		debugConstructionText = debugConstructionText + "(" + Convert.ToString(newVector3.x) + ", " + Convert.ToString(newVector3.y) + ", " + Convert.ToString(newVector3.z) + ")\n";
		UnityEngine.Debug.Log(newVector3);
	}

	public static void Log(bool newBool)
	{
		if (newBool == true)
		{
			debugConstructionText = debugConstructionText + "true\n";
		}
		else
		{
			debugConstructionText = debugConstructionText + "false\n";
		}
		UnityEngine.Debug.Log(newBool);
	}

	public static void Log(Enum newEnum)
	{
		debugConstructionText = debugConstructionText + newEnum.ToString() + "\n";
		UnityEngine.Debug.Log(newEnum);
	}
	#endregion
	
	void Start() {
		debugBoxStyle = new GUIStyle();
		debugBoxStyle.alignment = TextAnchor.UpperLeft;
		debugBoxStyle.fontSize = 24;
	}

	void Update()
	{
		if (debugConstructionText != "")
		{
			debugText = debugConstructionText;
			debugConstructionText = "";
		}
	}
	
	
	void OnGUI() {
		
		if (debugIsOn) {
			
			GUI.depth = 0;
			GUI.Box (new Rect (20, 20, Screen.width, Screen.height), debugText, debugBoxStyle);
		}
	}
	
	
}