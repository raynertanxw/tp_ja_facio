﻿public enum clawStateEnum {Idle, MoveJaws, MoveVertically, MoveHorizontally, Delay};

public enum menuStateEnum {None, Play, Prizes, Booth, Website};

public enum prizeTypeEnum {Logo, Pouch, Flap, Sling, Bag};

public enum GameMode {Normal, Booth};