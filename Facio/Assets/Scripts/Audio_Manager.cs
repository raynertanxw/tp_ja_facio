﻿using UnityEngine;
using System.Collections;

public class Audio_Manager : MonoBehaviour
{
	private static Audio_Manager sInstance = null;
	private static AudioSource[] sounds;
	private static int[] pausedSounds;

	private static bool bMusicEnabled = true;
	private static bool bSoundEffectEnabled = true;

	public static bool musicEnabled
	{
		get { return bMusicEnabled; }
		set
		{
			bMusicEnabled = value;
			if (value == true)
			{
				sounds[4].Play();
			}
			else
			{
				sounds[4].Stop();
			}
		}
	}
	public static bool soundEffectEnabled
	{
		get { return bSoundEffectEnabled; }
		set { bSoundEffectEnabled = value; }
	}

	void Awake()
	{
		if (sInstance == null)
		{
			setUp();
			DontDestroyOnLoad(this.gameObject);
			sInstance = this;
		}
		else
		{
			GameObject.Destroy(this.gameObject);
		}
	}

	void Start()
	{
		if (bMusicEnabled == false)
			sounds[4].Stop();
	}

	void Update()
	{
		if (Application.loadedLevel != 0)
		{
			Audio_Manager.PauseSoundEffects(GameManager.GetInstance.bIsPaused);
		}
	}

	public void setUp()
	{
		sounds = new AudioSource[transform.childCount];
		pausedSounds = new int[transform.childCount];
		for (int i = 0; i < transform.childCount; i++)
		{
			sounds[i] = transform.GetChild(i).GetComponent<AudioSource>();
			pausedSounds[i] = 0;
		}
	}











	#region Static sound buttons
	public static void PlayButtonSound()
	{
		if (bSoundEffectEnabled == true)
		{
			sounds[0].Play();
		}
	}

	public static void PlayClawLowerSound(bool _play)
	{
		if (bSoundEffectEnabled == true)
		{
			if (_play && GameManager.GetInstance.bIsPaused == false)
			{
				if (sounds[1].isPlaying == false)
					sounds[1].Play();
			}
			else
			{
				sounds[1].Stop();
			}
		}
	}

	public static void PlayPrizeGetSound()
	{
		if (bSoundEffectEnabled == true)
		{
			sounds[2].Play();
		}
	}

	public static void PlayClawMoveSound(bool _play)
	{
		if (bSoundEffectEnabled == true)
		{
			if (_play && GameManager.GetInstance.bIsPaused == false)
			{
				if (sounds[3].isPlaying == false)
					sounds[3].Play();
			}
			else
			{
				sounds[3].Stop();
			}
		}
	}

	public static void PlayClawOpenCloseSound()
	{
		if (bSoundEffectEnabled == true)
		{
			sounds[5].Play();
		}
	}

	public static void PauseSoundEffects(bool _paused)
	{
		if (_paused)
		{
			if (sounds[1].isPlaying == true)
			{
				pausedSounds[1] = 1;
				sounds[1].Stop();
			}
			if (sounds[3].isPlaying == true)
			{
				pausedSounds[3] = 1;
				sounds[3].Stop();
			}
		}
		else
		{
			for (int i = 0; i < pausedSounds.Length; i++)
			{
				if (pausedSounds[i] == 1)
				{
					pausedSounds[i] = 0;
					sounds[i].Play();
				}
			}
		}
	}

	public static void ResetPausedSounds()
	{
		for (int i = 0; i < pausedSounds.Length; i++)
		{
			pausedSounds[i] = 0;
		}
	}
	#endregion
}
