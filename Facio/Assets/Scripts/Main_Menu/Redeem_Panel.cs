﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Redeem_Panel : MonoBehaviour
{
	private Main_Menu_Buttons mainMenu;
	private InputField passwordField;
	private Text passwordPanelText;
	private Prizes_Panel prizePanel;
	
	private void Awake()
	{
		mainMenu = transform.parent.GetComponent<Main_Menu_Buttons>();
		passwordField = transform.GetChild(3).GetComponent<InputField>();
		passwordPanelText = transform.GetChild(2).GetComponent<Text>();
		prizePanel = GameObject.Find("Prizes Panel").GetComponent<Prizes_Panel>();
	}
	
	private void ResetPasswordPanel()
	{
		passwordField.text = "";
		passwordPanelText.text = "Please Enter\nRedeem Password:";
		passwordPanelText.color = Color.black;
	}

	private void Redeem()
	{
		transform.GetChild(3).gameObject.SetActive(false);
		transform.GetChild(2).GetComponent<RectTransform>().sizeDelta = new Vector2(1000, 500);
		transform.GetChild(2).GetComponent<RectTransform>().localPosition = new Vector3(0, 120, 0);
		passwordPanelText.text = "Congratulations!\nYou've earned yourself a $" + Prizes_Panel.numPrizes / 2 + "." + ((Prizes_Panel.numPrizes % 2) * 5) + "0 discount for your purchase of a Facio Bag! Enjoy!";
		PlayerPrefs.SetInt(Constants.redeemKey, 1);

		prizePanel.DisableRedeemButton();
	}

	#region Info Panel Buttons
	public void Button_Back()
	{
		Audio_Manager.PlayButtonSound();

		mainMenu.SetRedeemPanelVisibility(false);
		mainMenu.SetPanelVisibility(false);
		ResetPasswordPanel();
	}
	
	public void Button_OK()
	{
		Audio_Manager.PlayButtonSound();

		if (passwordField.text == "JA.tpfbi")
		{
			Redeem();
		}
		else
		{
			passwordPanelText.color = Color.red;
			passwordPanelText.text = "INVALID PASSWORD\nEnter Redeem Password:";
		}
	}
	#endregion
}
