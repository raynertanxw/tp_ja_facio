﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Prizes_Panel : MonoBehaviour
{
	public static int numPrizes = 0;

	private Main_Menu_Buttons mainMenu;
	public Sprite[] greyedOutPrizes;
	private Image[] prizes;
	private Button redeemButton;
	
	private void Awake()
	{
		mainMenu = transform.parent.GetComponent<Main_Menu_Buttons>();
		redeemButton = transform.GetChild(6).GetComponent<Button>();

		prizes = new Image[5];
		for (int i = 0; i < 5; i++)
		{
			prizes[i] = transform.GetChild(i).GetComponent<Image>();
		}

		Prizes_Panel.numPrizes = 0;
		CheckPrizes();

		// Disable redeem if already redeemed.
		if (PlayerPrefs.GetInt(Constants.redeemKey) == 1)
			redeemButton.interactable = false;
	}

	public void DisableRedeemButton()
	{
		redeemButton.interactable = false;
	}

	public void CheckPrizes()
	{
		for (int i = 0; i < 5; i++)
		{
			if (PlayerPrefs.HasKey(Constants.prizeKey[i]) == false)
			{
				prizes[i].sprite = greyedOutPrizes[i];
			}
			else if (PlayerPrefs.GetInt(Constants.prizeKey[i]) != 1)
			{
				prizes[i].sprite = greyedOutPrizes[i];
			}
			else
			{
				Prizes_Panel.numPrizes++;
			}
		}
	}
	
	#region Info Panel Buttons
	public void Button_Back()
	{
		Audio_Manager.PlayButtonSound();

		mainMenu.SetPrizesPanelVisibility(false);
		mainMenu.SetPanelVisibility(false);
		mainMenu.SetRedeemPanelVisibility(false);
	}

	public void Button_Redeem()
	{
		Audio_Manager.PlayButtonSound();

		mainMenu.SetRedeemPanelVisibility(true);
	}
	#endregion
}
