﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Recharge_Bar : MonoBehaviour
{
	private Text rechargeText, triesText;
	private Slider rechargeBar;

	private DateTime m_currentDate, m_oldDate;
	private TimeSpan m_difference;
	private int mnTries;
	public int numTries {get{return mnTries;}}
	
	void Awake()
	{
		rechargeBar = GetComponent<Slider>();
		rechargeText = transform.GetChild(2).GetComponent<Text>();
		triesText = transform.GetChild(3).GetComponent<Text>();
	}

	void Start()
	{
		if (PlayerPrefs.HasKey(Constants.LastRechargeTimeKey))
		{
			mnTries = DecodeTime();
			triesText.text = mnTries + "/5";
			if (mnTries < 5)
			{
				StartCoroutine(CheckRechargeBar());
			}
			else
			{
				rechargeBar.value = 1f;
				rechargeText.text = "00:00";
			}
		}
		else
		{
			gameObject.SetActive(false);
		}
	}



	#region Helper functions
	private int DecodeTime()
	{
		int retval_tries = PlayerPrefs.GetInt(Constants.triesKey);

		//Store the current time when it starts
		DateTime currentDate = System.DateTime.Now;
		//Grab the old time from the player prefs as a long
		Int64 tmp_oldDate = Convert.ToInt64(PlayerPrefs.GetString(Constants.LastRechargeTimeKey));
		//Convert the old time from binary to a DataTime variable
		DateTime oldDate = DateTime.FromBinary(tmp_oldDate);
		//Use the Subtract method and store the result as a timespan variable
		TimeSpan difference = currentDate.Subtract(oldDate);
		
		
		int secsSinceLastRecharge = Convert.ToInt32(difference.TotalSeconds);
		int numHours = secsSinceLastRecharge / Constants.secondsPerHour;
		retval_tries += numHours;
		
		if (retval_tries > 5)
		{
			retval_tries = 5;
		}
		else if (retval_tries < 5 && numHours > 0) // Bring over the leftover time.
		{
			DateTime newRechargeTime = System.DateTime.Now;
			newRechargeTime.AddSeconds(-(secsSinceLastRecharge % Constants.secondsPerHour));
			PlayerPrefs.SetString(Constants.LastRechargeTimeKey, newRechargeTime.ToBinary().ToString());
			PlayerPrefs.SetInt(Constants.triesKey, retval_tries);
		}

		return retval_tries;
	}
	#endregion

	public void ImmediateRefill()
	{
		mnTries = 5;
		PlayerPrefs.SetInt(Constants.triesKey, mnTries);
		triesText.text = mnTries + "/5";
		rechargeText.text = "00:00";
		rechargeBar.value = 1f;
	}

	private IEnumerator CheckRechargeBar()
	{
		// Cache old date.
		Int64 tmp_oldDate = Convert.ToInt64(PlayerPrefs.GetString(Constants.LastRechargeTimeKey));
		m_oldDate = DateTime.FromBinary(tmp_oldDate);

		int secsSinceLastRecharge, numHours, secsLeftTillRecharge;
		int displayMin, displaySec;

		while (true)
		{
			m_currentDate = System.DateTime.Now;
			m_difference = m_currentDate.Subtract(m_oldDate);

			secsSinceLastRecharge = Convert.ToInt32(m_difference.TotalSeconds);
			numHours = secsSinceLastRecharge / Constants.secondsPerHour;
			mnTries += numHours;
			if (mnTries > 5)
				mnTries = 5;

			if (mnTries == 5)
			{
				PlayerPrefs.SetInt(Constants.triesKey, mnTries);
				triesText.text = mnTries + "/5";
				rechargeText.text = "00:00";
				rechargeBar.value = 1f;
				break;
			}
			else if (numHours > 0)
			{
				DateTime newRechargeTime = System.DateTime.Now;
				PlayerPrefs.SetString(Constants.LastRechargeTimeKey, newRechargeTime.ToBinary().ToString());
				PlayerPrefs.SetInt(Constants.triesKey, mnTries);
				m_oldDate = newRechargeTime;

				// Update UI.
				triesText.text = mnTries + "/5";
				rechargeText.text = "00:00";
				rechargeBar.value = 1f;
			}
			else
			{
				rechargeBar.value = (float) Convert.ToInt32(m_difference.TotalSeconds) / (float) Constants.secondsPerHour;
				secsLeftTillRecharge = Constants.secondsPerHour - Convert.ToInt32(m_difference.TotalSeconds);
				displayMin = secsLeftTillRecharge / 60;
				displaySec = secsLeftTillRecharge % 60;
				if (displayMin < 10)
				{
					if (displaySec < 10)
						rechargeText.text = "0" + displayMin + ":0" + displaySec;
					else
						rechargeText.text = "0" + displayMin + ":" + displaySec;
				}
				else
				{
					if (displaySec < 10)
						rechargeText.text = displayMin + ":0" + displaySec;
					else
						rechargeText.text = displayMin + ":" + displaySec;
				}
			}

			yield return null;
		}
	}
}
