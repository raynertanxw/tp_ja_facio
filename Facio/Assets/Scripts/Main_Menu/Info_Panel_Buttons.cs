﻿using UnityEngine;
using System.Collections;
using System;

public class Info_Panel_Buttons : MonoBehaviour
{
	private Main_Menu_Buttons mainMenu;
	private Recharge_Bar rechargeBar;

	private void Awake()
	{
		mainMenu = transform.parent.GetComponent<Main_Menu_Buttons>();
		rechargeBar = mainMenu.transform.GetChild(6).GetComponent<Recharge_Bar>();
	}

	#region Info Panel Buttons
	public void Button_Back()
	{
		Audio_Manager.PlayButtonSound();

		mainMenu.SetPanelVisibility(false);
	}

	public void Button_OK()
	{
		switch (mainMenu.menuState)
		{
		case menuStateEnum.Play:
			Audio_Manager.PlayButtonSound();
			PlayerPrefs.SetInt(Constants.HasPlayedBeforeKey, 1);
			Application.LoadLevel(1);
			break;
		case menuStateEnum.Prizes:
			Audio_Manager.PlayButtonSound();
			mainMenu.SetPrizesPanelVisibility(true);
			break;
		case menuStateEnum.Booth:
			Audio_Manager.PlayButtonSound();
			mainMenu.SetPasswordPanelVisibility(true);
			break;
		case menuStateEnum.Website:
			AttemptGiveRefill();
			Application.OpenURL("https://www.facebook.com/facioja/");
			break;
		}
	}
	#endregion

	#region Facebook free refill helper functions
	private void AttemptGiveRefill()
	{
		if (PlayerPrefs.HasKey(Constants.HasUsedFacebookRefillKey) == true)
			return;
		else if (rechargeBar.numTries == 0)
		{
			GiveRefill();
		}
	}

	private void GiveRefill()
	{
		rechargeBar.ImmediateRefill();
		PlayerPrefs.SetInt(Constants.HasUsedFacebookRefillKey, 1);
	}
	#endregion
}
