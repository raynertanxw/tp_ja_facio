﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Main_Menu_Buttons : MonoBehaviour
{
	private CanvasGroup infoPanel, passwordPanel, prizesPanel, redeemPanel;
	public menuStateEnum menuState = menuStateEnum.None;
	private Info_Panel_Buttons infoPanelButtons;
	private Text infoText;
	private Image musicButtonRen, soundEffectButtonRen;
	public Sprite musicOn, musicOff, soundEffectOn, soundEffectOff;

	private void Awake()
	{
		infoPanel = GameObject.Find("Information Panel").GetComponent<CanvasGroup>();
		infoText = GameObject.Find("Information_Text").GetComponent<Text>();
		passwordPanel = GameObject.Find("Booth Password Panel").GetComponent<CanvasGroup>();
		prizesPanel = GameObject.Find("Prizes Panel").GetComponent<CanvasGroup>();
		redeemPanel = GameObject.Find("Redeem Password Panel").GetComponent<CanvasGroup>();
		musicButtonRen = GameObject.Find("Music_Button").GetComponent<Image>();
		soundEffectButtonRen = GameObject.Find("SoundEffect_Button").GetComponent<Image>();
		SetPanelVisibility(false);
		SetPasswordPanelVisibility(false);
		SetPrizesPanelVisibility(false);
		SetRedeemPanelVisibility(false);

		if (PlayerPrefs.HasKey(Constants.MusicEnabledKey))
		{
			if (PlayerPrefs.GetInt(Constants.MusicEnabledKey) == 0)
			{
				SetMusicEnabled(false);
			}
		}
		else
		{
			PlayerPrefs.SetInt(Constants.MusicEnabledKey, 1);
		}
		
		if (PlayerPrefs.HasKey(Constants.SoundEffectEnabledKey))
		{
			if (PlayerPrefs.GetInt(Constants.SoundEffectEnabledKey) == 0)
			{
				SetSoundEffectsEnabled(false);
			}
		}
		else
		{
			PlayerPrefs.SetInt(Constants.SoundEffectEnabledKey, 1);
		}
	}

	#region SetVisibility Functions
	public void SetPanelVisibility(bool visible)
	{
		if (visible == true)
		{
			infoPanel.alpha = 1;
			infoPanel.interactable = true;
			infoPanel.blocksRaycasts = true;
		}
		else
		{
			infoPanel.alpha = 0;
			infoPanel.interactable = false;
			infoPanel.blocksRaycasts = false;
		}
	}

	public void SetPasswordPanelVisibility(bool visible)
	{
		if (visible == true)
		{
			passwordPanel.alpha = 1;
			passwordPanel.interactable = true;
			passwordPanel.blocksRaycasts = true;
		}
		else
		{
			passwordPanel.alpha = 0;
			passwordPanel.interactable = false;
			passwordPanel.blocksRaycasts = false;
		}
	}

	public void SetPrizesPanelVisibility(bool visible)
	{
		if (visible == true)
		{
			prizesPanel.alpha = 1;
			prizesPanel.interactable = true;
			prizesPanel.blocksRaycasts = true;
		}
		else
		{
			prizesPanel.alpha = 0;
			prizesPanel.interactable = false;
			prizesPanel.blocksRaycasts = false;
		}
	}

	public void SetRedeemPanelVisibility(bool visible)
	{
		if (visible == true)
		{
			redeemPanel.alpha = 1;
			redeemPanel.interactable = true;
			redeemPanel.blocksRaycasts = true;
		}
		else
		{
			redeemPanel.alpha = 0;
			redeemPanel.interactable = false;
			redeemPanel.blocksRaycasts = false;
		}
	}
	#endregion



	#region Main Menu Buttons
	public void Button_Play()
	{
		Audio_Manager.PlayButtonSound();

		menuState = menuStateEnum.Play;
		if (PlayerPrefs.HasKey(Constants.HasPlayedBeforeKey) == true)
		{
			Application.LoadLevel(1);
		}
		else
		{
			infoText.text = "Welcome! You will start with five tries. Each try will take an hour to recharge. Get as many types of prizes as you can! You can check the prizes you have gotten by clicking PRIZES. Please enjoy the game!";
			SetPanelVisibility(true);
		}
	}

	public void Button_Prizes()
	{
		Audio_Manager.PlayButtonSound();

		menuState = menuStateEnum.Prizes;
		if (PlayerPrefs.GetInt(Constants.redeemKey) == 1)
			infoText.text = "You've already redeemed your discount. Click OK to view the prizes you have collected but you cannot redeem them anymore. Thank you for playing and enjoy your Facio bag!";
		else
			infoText.text = "View the types of prizes that you have collected. Each type of prize is equivilent to a $0.50 discount for a Facio Bag! Click FACEBOOK in the main menu for more details. Click OK to view your prizes.";
		SetPanelVisibility(true);
	}

	public void Button_Booth()
	{
		Audio_Manager.PlayButtonSound();

		menuState = menuStateEnum.Booth;
		infoText.text = "This game mode is only used at Facio product booths only.";
		SetPanelVisibility(true);
	}

	public void Button_Website()
	{
		Audio_Manager.PlayButtonSound();

		menuState = menuStateEnum.Website;
		infoText.text = "To visit our Facio Facebook page for more product information, click OK";
		SetPanelVisibility(true);
	}

	public void ButtonToggleMusic()
	{
		SetMusicEnabled(!Audio_Manager.musicEnabled);
		Audio_Manager.PlayButtonSound();
	}
	
	public void ButtonToggleSoundEffect()
	{
		SetSoundEffectsEnabled(!Audio_Manager.soundEffectEnabled);
		Audio_Manager.PlayButtonSound();
	}
	#endregion

	public void SetMusicEnabled(bool _enabled)
	{
		Audio_Manager.musicEnabled = _enabled;
		if (_enabled == true)
		{
			musicButtonRen.sprite = musicOn;
			PlayerPrefs.SetInt(Constants.MusicEnabledKey, 1);
		}
		else 
		{
			musicButtonRen.sprite = musicOff;
			PlayerPrefs.SetInt(Constants.MusicEnabledKey, 0);
		}
	}
	
	public void SetSoundEffectsEnabled(bool _enabled)
	{
		Audio_Manager.soundEffectEnabled = _enabled;
		if (_enabled == true)
		{
			soundEffectButtonRen.sprite = soundEffectOn;
			PlayerPrefs.SetInt(Constants.SoundEffectEnabledKey, 1);
		}
		else
		{
			soundEffectButtonRen.sprite = soundEffectOff;
			PlayerPrefs.SetInt(Constants.SoundEffectEnabledKey, 0);
		}
	}
}
