﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Booth_Panel_Buttons : MonoBehaviour
{
	private Main_Menu_Buttons mainMenu;
	private InputField passwordField;
	private Text passwordPanelText;
	
	private void Awake()
	{
		mainMenu = transform.parent.GetComponent<Main_Menu_Buttons>();
		passwordField = transform.GetChild(3).GetComponent<InputField>();
		passwordPanelText = transform.GetChild(2).GetComponent<Text>();
	}

	private void ResetPasswordPanel()
	{
		passwordField.text = "";
		passwordPanelText.text = "Please Enter\nBooth Password:";
		passwordPanelText.color = Color.black;
	}

	#region Info Panel Buttons
	public void Button_Back()
	{
		Audio_Manager.PlayButtonSound();

		mainMenu.SetPasswordPanelVisibility(false);
		mainMenu.SetPanelVisibility(false);
		ResetPasswordPanel();
	}
	
	public void Button_OK()
	{
		Audio_Manager.PlayButtonSound();

		if (passwordField.text == "JA.tpfbi")
		{
			Application.LoadLevel(2);
		}
//		else if (passwordField.text == "hardreset")
//		{
//			PlayerPrefs.DeleteAll();
//			Application.LoadLevel(0);
//		}
		else
		{
			passwordPanelText.color = Color.red;
			passwordPanelText.text = "INVALID PASSWORD\nEnter Booth Password:";
		}
	}
	#endregion
}
